package id.co.iconpln.controlflowapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import kotlinx.android.synthetic.main.activity_intent_with_result.*

class IntentWithResult : AppCompatActivity(), View.OnClickListener {

    companion object {
        const val EXTRA_VALUE = "extra_value"
        const val RESULT_CODE = 110
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intent_with_result)

        btnResultChoose.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        if (v.id == R.id.btnResultChoose) {
            if (rgNumber.checkedRadioButtonId != 0) {
                val value = when (rgNumber.checkedRadioButtonId) {
                    R.id.rb50 -> 50
                    R.id.rb100 -> 100
                    R.id.rb150 -> 150
                    R.id.rb200 -> 200
                    else -> 0
                }

                val resultIntent = Intent()
                resultIntent.putExtra(EXTRA_VALUE, value)
                setResult(RESULT_CODE, resultIntent)
                finish()
            }
        }

    }
}
