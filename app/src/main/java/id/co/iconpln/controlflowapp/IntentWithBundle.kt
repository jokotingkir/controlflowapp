package id.co.iconpln.controlflowapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_intent_with_data.*

class IntentWithBundle : AppCompatActivity() {

    companion object {
        const val EXTRA_AGE = "extra_age"
        const val EXTRA_NAME = "extra_name"
    }

    private var name: String = ""
    private var age: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intent_with_bundle)

        getIntentExtra()
        showData()
    }

    private fun getIntentExtra() {
        val bundle = intent.extras
        name = bundle?.getString(EXTRA_NAME) ?: ""
        age = bundle?.getInt(EXTRA_AGE) ?: 0

    }

    private fun showData() {
        val text = "Name: $name, Age: $age"
        tvDataReceived.text = text
    }
}
