package id.co.iconpln.controlflowapp.myProfileRegister

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.model.myProfile.RegisterUser
import kotlinx.android.synthetic.main.activity_my_profile_register.*

class MyProfileRegisterActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var registerViewModel: ProfileRegisterViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_profile_register)

        initViewModel()
        btnProfileRegister.setOnClickListener(this)
    }

    private fun initViewModel() {
        registerViewModel = ViewModelProvider(this, ViewModelProvider.NewInstanceFactory()).get(
            ProfileRegisterViewModel::class.java
        )
    }

    override fun onClick(view: View) {
        if (view.id == R.id.btnProfileRegister) {
            val newUser = RegisterUser(
                etProfileRegEmail.text.toString(),
                etProfileRegPassword.text.toString(),
                etProfileRegName.text.toString(),
                etProfileRegHp.text.toString()
            )

            registerUser(newUser)
        }
    }

    private fun registerUser(newUser: RegisterUser) {
        showLoading(true)
        registerViewModel.doRegister(newUser).observe(this, Observer { response ->
            if (response != null) {
                Toast.makeText(this, response.messages[0], Toast.LENGTH_SHORT).show()
            }
            showLoading(false)
        })
    }

    private fun showLoading(show: Boolean) {
        if (show) {
            btnProfileRegister.visibility = View.GONE
            pbLoadingRegister.visibility = View.VISIBLE
        } else {
            btnProfileRegister.visibility = View.VISIBLE
            pbLoadingRegister.visibility = View.GONE
        }
    }


}
