package id.co.iconpln.controlflowapp.myContact

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import id.co.iconpln.controlflowapp.R
import kotlinx.android.synthetic.main.activity_my_contact.*
import androidx.lifecycle.Observer

class MyContactActivity : AppCompatActivity() {

    private lateinit var adapter: MyContactAdapter
    private lateinit var myContactVm: MyContactVM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_contact)

        initViewModel()
        showMyContactList()

        fetchContactData()
    }

    private fun fetchContactData() {
        myContactVm.getListContacts().observe(this, Observer { contactItem ->
            if (contactItem != null) {
                adapter.setData(contactItem)
            } else {
                Toast.makeText(this, "Failed to Load Data", Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun showMyContactList() {
        adapter = MyContactAdapter()
        adapter.notifyDataSetChanged()

        rvMyContactList.layoutManager = LinearLayoutManager(this)
        rvMyContactList.adapter = adapter
    }

    private fun initViewModel() {
        myContactVm = ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(MyContactVM::class.java)
    }
}
