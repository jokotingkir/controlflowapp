package id.co.iconpln.controlflowapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.activity_operation.*

class OperationActivity : AppCompatActivity(), View.OnClickListener {

    private var inputX: Double = 0.0
    private var inputY: Double = 0.0

    private lateinit var operationViewModel: OperationViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_operation)

        initViewModel()
        displayResult()
        setButtonClickListener()
    }

    private fun displayResult() {
        tvOpResult.text = operationViewModel.operationResult.toString()
        tvOperator.text = operationViewModel.whichOperator
    }

    private fun initViewModel() {
        operationViewModel = ViewModelProviders.of(this).get(OperationViewModel::class.java)
    }

    private fun getInputNumbers() {
        if (etBilanganX.text?.isNotEmpty() == true && etBilanganY.text?.isNotEmpty() == true) {
            inputX = etBilanganX.text.toString().toDouble()
            inputY = etBilanganY.text.toString().toDouble()
        } else {
            etBilanganX.setText(getString(R.string.op_result))
            etBilanganY.setText(getString(R.string.op_result))
        }
    }

    private fun setButtonClickListener() {
        btnOpAdd.setOnClickListener(this)
        btnOpDivide.setOnClickListener(this)
        btnOpMultiply.setOnClickListener(this)
        btnOpSubstract.setOnClickListener(this)
        btnReset.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnOpAdd -> {
                operationViewModel.whichOperator = getString(R.string.add)
                getInputNumbers()
                val op = Operation.Add(inputX)
                operationViewModel.execute(inputY, op)
                displayResult()
            }

            R.id.btnOpDivide -> {
                operationViewModel.whichOperator = getString(R.string.divide)
                getInputNumbers()
                val op = Operation.Divide(inputX)
                operationViewModel.execute(inputY, op)
                displayResult()
            }

            R.id.btnOpMultiply -> {
                operationViewModel.whichOperator = getString(R.string.multiply)
                getInputNumbers()
                val op = Operation.Multiply(inputX)
                operationViewModel.execute(inputY, op)
                displayResult()
            }

            R.id.btnOpSubstract -> {
                operationViewModel.whichOperator = getString(R.string.substract)
                getInputNumbers()
                val op = Operation.Substract(inputX)
                operationViewModel.execute(inputY, op)
                displayResult()
            }

            R.id.btnReset -> {
                operationViewModel.operationResult = 0.0
                operationViewModel.whichOperator = getString(R.string.add)
                displayResult()
                etBilanganX.setText(getString(R.string.op_result))
                etBilanganY.setText(getString(R.string.op_result))
            }
        }
    }
}
