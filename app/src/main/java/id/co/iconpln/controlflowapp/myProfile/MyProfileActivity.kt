package id.co.iconpln.controlflowapp.myProfile

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.model.myProfile.ProfileLoginResponse
import id.co.iconpln.controlflowapp.model.myProfile.ProfileResponse
import id.co.iconpln.controlflowapp.model.myProfile.ProfileUser
import id.co.iconpln.controlflowapp.model.myProfile.ProfileUserPreference
import id.co.iconpln.controlflowapp.myProfileLogin.MyProfileLoginActivity
import kotlinx.android.synthetic.main.activity_my_profile.*

class MyProfileActivity : AppCompatActivity(), View.OnClickListener {

    companion object {
        private const val REQUEST_CODE = 200
    }

    private lateinit var myProfileViewModel: MyProfileViewModel
    private var profileLoginResponse: ProfileLoginResponse<ProfileResponse?>? = null
    private lateinit var profileUserPreference: ProfileUserPreference
    private lateinit var profileUser: ProfileUser

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_profile)

        initViewModel()
        setOnClickListener()
        showExistingPreference()
    }

    private fun fetchUserData(token: String) {
        pbProfileLoading.visibility = View.VISIBLE
        llProfileContent.visibility = View.GONE
        myProfileViewModel.getUser(token).observe(this, Observer { result ->
            if (result != null) {
                if (result.success) {
                    pbProfileLoading.visibility = View.GONE
                    llProfileContent.visibility = View.VISIBLE

                    tvProfileId.text = result.data?.id.toString()
                    tvProfileName.text = result.data?.name
                    tvProfileEmail.text = result.data?.email
                    tvProfilePhone.text = result.data?.phone

                    tvProfileWarning.visibility = View.GONE
                    btnProfileToLogin.visibility = View.GONE
                    btnProfileLogout.visibility = View.VISIBLE
                } else {
                    pbProfileLoading.visibility = View.GONE
                    llProfileContent.visibility = View.VISIBLE
                    tvProfileWarning.text = result.messages.joinToString(",")
                }
            } else {
                pbProfileLoading.visibility = View.GONE
                profileUserPreference.removeProfileUser(profileUser)
                showLogoutProfile()
                Toast.makeText(this, "Network Failure", Toast.LENGTH_LONG).show()
            }
        })
    }

    private fun initViewModel() {
        myProfileViewModel = ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(MyProfileViewModel::class.java)
    }

    private fun setOnClickListener() {
        btnProfileToLogin.setOnClickListener(this)
        btnProfileLogout.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        if (view.id == R.id.btnProfileToLogin) {
            val loginIntent = Intent(this, MyProfileLoginActivity::class.java)
            startActivityForResult(loginIntent, REQUEST_CODE)
        } else {
            profileUserPreference.removeProfileUser(profileUser)
            showLogoutProfile()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_CODE) {
            if (resultCode == MyProfileLoginActivity.RESULT_CODE) {
                profileLoginResponse =
                    data?.getParcelableExtra(MyProfileLoginActivity.EXTRA_PROFILE_RESULT) as ProfileLoginResponse<ProfileResponse?>

                saveProfileUserPreference()
            }
        }
    }

    private fun saveProfileUserPreference() {
        if (profileLoginResponse != null) {
            profileUser.userToken = profileLoginResponse?.token
            profileUserPreference.setProfileUser(profileUser)
            Toast.makeText(this, "Token saved", Toast.LENGTH_SHORT).show()
            showExistingPreference()
        }
    }

    private fun showExistingPreference() {
        profileUserPreference = ProfileUserPreference(this)
        profileUser = profileUserPreference.getProfileUser()

        val token = profileUserPreference.getProfileUser().userToken
        if (!token.isNullOrEmpty()) {
            fetchUserData(token)
        } else {
            showLogoutProfile()
        }
    }

    private fun showLogoutProfile() {
        pbProfileLoading.visibility = View.GONE
        llProfileContent.visibility = View.VISIBLE
        tvProfileWarning.visibility = View.VISIBLE
        btnProfileToLogin.visibility = View.VISIBLE
        btnProfileLogout.visibility = View.GONE

        tvProfileId.text = resources.getString(R.string.empty)
        tvProfileName.text = resources.getString(R.string.empty)
        tvProfileEmail.text = resources.getString(R.string.empty)
        tvProfilePhone.text = resources.getString(R.string.empty)

    }
}
