package id.co.iconpln.controlflowapp.ui.quotes

import androidx.lifecycle.ViewModel
import id.co.iconpln.controlflowapp.data.Quote
import id.co.iconpln.controlflowapp.data.QuoteRepository

class QuotesViewModel(private val quoteRepository: QuoteRepository) : ViewModel() {

    fun getQuotes() = quoteRepository.getQuotes()

    fun addQuote(quote: Quote) = quoteRepository.addQuote(quote)
}