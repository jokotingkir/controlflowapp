package id.co.iconpln.controlflowapp.myUserForm

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import id.co.iconpln.controlflowapp.model.myUser.UserDataResponse
import id.co.iconpln.controlflowapp.network.MyNetworkUserRepository

class MyUserFormVM : ViewModel() {
    fun updateUser(id: Int, userData: UserDataResponse): MutableLiveData<UserDataResponse> {
        return MyNetworkUserRepository().updateUser(id, userData)
    }

    fun deleteUser(id: Int): MutableLiveData<UserDataResponse> {
        return MyNetworkUserRepository().deleteUser(id)
    }

    fun createUser(userData: UserDataResponse): MutableLiveData<UserDataResponse> {
        return MyNetworkUserRepository().addUser(userData)
    }

    fun getUser(id: Int): MutableLiveData<UserDataResponse> {
        return MyNetworkUserRepository().getSingleUser(id)
    }
}