package id.co.iconpln.controlflowapp

import android.annotation.SuppressLint
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.math.pow

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val expo = et_expo.text
        val num = et_nilai.text

        btn_show.setOnClickListener {
            if (expo.isNullOrEmpty() || num.isNullOrEmpty()) {
                showToast("Please fill both of value (Nilai & Eksponen)")
            } else {
                calcExponent(num.toString().toDouble(), expo.toString().toDouble())
            }
            closeKeyboard()
        }

        btn_reset.setOnClickListener {
            resetInput()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun calcExponent(n: Double, e: Double) {
        val resCalc = n.pow(e)

        result_text.text = "${n.toInt()} ^ ${e.toInt()} = ${resCalc.toInt()}"
    }

    fun showToast(msg: String) {
        Toast.makeText(
            this,
            msg,
            Toast.LENGTH_LONG
        ).show()
    }

    private fun closeKeyboard() {
        val imm: InputMethodManager =
            getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(currentFocus?.windowToken, InputMethodManager.SHOW_FORCED)
    }

    private fun resetInput() {
        et_nilai.text.clear()
        et_expo.text.clear()
        result_text.text = null
        et_nilai.requestFocus()
        closeKeyboard()

        showToast("input has been reset")
    }
}
