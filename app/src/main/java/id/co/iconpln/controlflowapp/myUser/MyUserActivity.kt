package id.co.iconpln.controlflowapp.myUser

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.database.FavoriteViewModel
import id.co.iconpln.controlflowapp.model.myUser.UserDataResponse
import id.co.iconpln.controlflowapp.myUserForm.MyUserFormActivity
import kotlinx.android.synthetic.main.activity_my_user.*

class MyUserActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var adapter: MyUserAdapter
    private lateinit var myUserVm: MyUserVM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_user)

        initViewModel()
        showMyUserList()

        fetchContactData()
        fabMyUserAdd.setOnClickListener(this)
    }

    override fun onResume() {
        super.onResume()
        fetchContactData()
    }

    private fun fetchContactData() {
        showLoading(true)
        myUserVm.getListUser().observe(this, Observer { userItem ->
            if (userItem != null) {
                var sortedList = userItem.sortedWith(compareBy { it.id }).asReversed()
                adapter.setData(ArrayList(sortedList))
            } else {
                Toast.makeText(this, "Failed to load Contact Data", Toast.LENGTH_SHORT).show()
            }
            showLoading(false)
        })
    }

    private fun initViewModel() {
        myUserVm = ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(MyUserVM::class.java)
    }

    private fun showMyUserList() {
        adapter = MyUserAdapter()
        adapter.notifyDataSetChanged()

        rvMyUserList.layoutManager = LinearLayoutManager(this)
        rvMyUserList.adapter = adapter

        adapter.setOnItemClickCallback(object : MyUserAdapter.OnItemClickCallback {
            override fun onItemClick(user: UserDataResponse) {
                val userIntent = Intent(this@MyUserActivity, MyUserFormActivity::class.java)

                userIntent.putExtra(MyUserFormActivity.EXTRA_USER_ID, user.id)
                userIntent.putExtra(MyUserFormActivity.EXTRA_USER_EDIT, true)
                startActivity(userIntent)
            }

        })
    }

    override fun onClick(view: View) {
        if (view.id == R.id.fabMyUserAdd) {
            val userForm = Intent(this@MyUserActivity, MyUserFormActivity::class.java)
            startActivity(userForm)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_list_favorite, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        showFavoriteList(item.itemId)
        return super.onOptionsItemSelected(item)
    }

    private fun showFavoriteList(itemId: Int) {
        if (itemId == R.id.listFavorite) {
            val favIntent = Intent(this, MyUserFavoriteActivity::class.java)
            startActivity(favIntent)
        }
    }

    private fun showLoading(visible: Boolean) {
        pbMyUser.visibility = if (visible) View.VISIBLE else View.GONE
    }
}
