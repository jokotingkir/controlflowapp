package id.co.iconpln.controlflowapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import id.co.iconpln.controlflowapp.hero.GridHeroAdapter
import id.co.iconpln.controlflowapp.model.Hero
import id.co.iconpln.controlflowapp.model.HeroesData
import kotlinx.android.synthetic.main.activity_grid_hero2.*

class GridHeroActivity : AppCompatActivity() {

    private var gridHero: ArrayList<Hero> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_grid_hero2)

        setupGridHero()
        showRecyclerGrid()
    }

    private fun setupGridHero() {
        rvGridHero.setHasFixedSize(true)
        gridHero.addAll(HeroesData.listDataHero)
    }

    private fun showRecyclerGrid() {
        rvGridHero.layoutManager = GridLayoutManager(this, 3)
        val gridHeroAdapter = GridHeroAdapter(gridHero)
        rvGridHero.adapter = gridHeroAdapter

        gridHeroAdapter.setOnItemClickCallback(object : GridHeroAdapter.OnItemClickCallBack {
            override fun onItemClick(hero: Hero) {
                Toast.makeText(this@GridHeroActivity, hero.name, Toast.LENGTH_SHORT).show()
            }
        })
    }
}
