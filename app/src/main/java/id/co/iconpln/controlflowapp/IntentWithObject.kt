package id.co.iconpln.controlflowapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import id.co.iconpln.controlflowapp.model.Person
import kotlinx.android.synthetic.main.activity_intent_with_object.*

class IntentWithObject : AppCompatActivity() {

    companion object {
        var PERSON = "person"
    }

    private lateinit var p: Person

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intent_with_object)

        getIntentExtras()
        showData()
    }


    private fun getIntentExtras() {
        p = intent.getParcelableExtra(PERSON)
    }

    private fun showData() {
        val text = "Name: ${p.name}, Age: ${p.age}, Email: ${p.email}, City: ${p.city}"
        tvDataReceived.text = text
    }
}
