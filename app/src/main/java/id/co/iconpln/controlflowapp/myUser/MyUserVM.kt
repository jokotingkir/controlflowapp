package id.co.iconpln.controlflowapp.myUser

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import id.co.iconpln.controlflowapp.model.myUser.UserDataResponse
import id.co.iconpln.controlflowapp.network.MyNetworkUserRepository

class MyUserVM : ViewModel() {
    fun getListUser(): MutableLiveData<ArrayList<UserDataResponse>> {
        return MyNetworkUserRepository().getAllUser()
    }
}