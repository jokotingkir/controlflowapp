package id.co.iconpln.controlflowapp.contactFragment


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.contact.ContactAdapter
import id.co.iconpln.controlflowapp.contact.ContactViewModel
import kotlinx.android.synthetic.main.activity_weather.*
import kotlinx.android.synthetic.main.fragment_contact.*

/**
 * A simple [Fragment] subclass.
 */
class ContactFragment : Fragment() {

    private lateinit var contactAdapter: ContactAdapter
    private lateinit var contactViewModel: ContactViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_contact, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViewModel()
        showContactList()

        contactViewModel.setContact()
        fetchContactData()
    }

    private fun fetchContactData() {
        showLoading(true)
        contactViewModel.getContact().observe(this, Observer { contactItem ->
            if (contactItem != null) {
                contactAdapter.setData(contactItem)
                showLoading(false)
            }
        })
    }

    private fun showContactList() {
        contactAdapter = ContactAdapter()
        contactAdapter.notifyDataSetChanged()

        rvContactListFragment.layoutManager = LinearLayoutManager(requireContext())
        rvContactListFragment.adapter = contactAdapter
    }

    private fun initViewModel() {
        contactViewModel = ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(ContactViewModel::class.java)
    }

    private fun showLoading(state: Boolean) = if (state) {
        pbContactFragment.visibility = View.VISIBLE
    } else {
        pbContactFragment.visibility = View.GONE
    }
}
