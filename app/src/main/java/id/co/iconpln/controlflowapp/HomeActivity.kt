package id.co.iconpln.controlflowapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import id.co.iconpln.controlflowapp.sharedPreference.SharedPreferencesActivity
import id.co.iconpln.controlflowapp.bottomSheetDialog.BottomSheetActivity
import id.co.iconpln.controlflowapp.contact.ContactActivity
import id.co.iconpln.controlflowapp.contactFragment.ContactTabActivity
import id.co.iconpln.controlflowapp.fragmentBottomNav.BottomNavActivity
import id.co.iconpln.controlflowapp.fragmentNavDrawer.NavDrawerActivity
import id.co.iconpln.controlflowapp.fragmentTab.TabActivity
import id.co.iconpln.controlflowapp.fragmentViewPager.ScrollActivity
import id.co.iconpln.controlflowapp.fragments.DemoFragmentActivity
import id.co.iconpln.controlflowapp.hero.ListHeroActivity
import id.co.iconpln.controlflowapp.myContact.MyContactActivity
import id.co.iconpln.controlflowapp.myProfile.MyProfileActivity
import id.co.iconpln.controlflowapp.myProfileLogin.MyProfileLoginActivity
import id.co.iconpln.controlflowapp.myUser.MyUserActivity
import id.co.iconpln.controlflowapp.ui.quotes.QuotesActivity
import id.co.iconpln.controlflowapp.weather.WeatherActivity
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity(), View.OnClickListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        setOnClickButton()
    }

    private fun setOnClickButton() {
        btnCalculation.setOnClickListener(this)
        btnClassification.setOnClickListener(this)
        btnAccessLogin.setOnClickListener(this)
        btnToOperation.setOnClickListener(this)
        btnToStyle.setOnClickListener(this)
        btnDemoActivity.setOnClickListener(this)
        btnActivityVolume.setOnClickListener(this)
        btnIntent.setOnClickListener(this)
        btnConstraint.setOnClickListener(this)
        btnComplexConstraint.setOnClickListener(this)
        btnListHero.setOnClickListener(this)
        btnGridHero.setOnClickListener(this)
        btnDemoFragment.setOnClickListener(this)
        btnTab.setOnClickListener(this)
        btnBottomNav.setOnClickListener(this)
        btnNavDrawer.setOnClickListener(this)
        btnBottomSheet.setOnClickListener(this)
        btnScrollActivity.setOnClickListener(this)
        btnLocalization.setOnClickListener(this)
        btnQuotes.setOnClickListener(this)
        btnSharedPreference.setOnClickListener(this)
        btnWeather.setOnClickListener(this)
        btnContact.setOnClickListener(this)
        btnBgThread.setOnClickListener(this)
        btnContactTab.setOnClickListener(this)
        btnMyContact.setOnClickListener(this)
        btnMyUser.setOnClickListener(this)
        btnProfileLogin.setOnClickListener(this)
        btnToProfile.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        val i: Intent = when (view.id) {
            R.id.btnCalculation -> Intent(this, MainActivity::class.java)
            R.id.btnClassification -> Intent(this, ClassificationActivity::class.java)
            R.id.btnAccessLogin -> Intent(this, LoginActivity::class.java)
            R.id.btnToStyle -> Intent(this, StyleActivity::class.java)
            R.id.btnDemoActivity -> Intent(this, DemoActivity::class.java)
            R.id.btnActivityVolume -> Intent(this, VolumeActivity::class.java)
            R.id.btnIntent -> Intent(this, IntentActivity::class.java)
            R.id.btnConstraint -> Intent(this, ConstraintActivity::class.java)
            R.id.btnComplexConstraint -> Intent(this, ComplexConstraintActivity::class.java)
            R.id.btnListHero -> Intent(this, ListHeroActivity::class.java)
            R.id.btnGridHero -> Intent(this, GridHeroActivity::class.java)
            R.id.btnDemoFragment -> Intent(this, DemoFragmentActivity::class.java)
            R.id.btnTab -> Intent(this, TabActivity::class.java)
            R.id.btnBottomNav -> Intent(this, BottomNavActivity::class.java)
            R.id.btnNavDrawer -> Intent(this, NavDrawerActivity::class.java)
            R.id.btnBottomSheet -> Intent(this, BottomSheetActivity::class.java)
            R.id.btnScrollActivity -> Intent(this, ScrollActivity::class.java)
            R.id.btnLocalization -> Intent(this, LocalizationActivity::class.java)
            R.id.btnQuotes -> Intent(this, QuotesActivity::class.java)
            R.id.btnSharedPreference -> Intent(this, SharedPreferencesActivity::class.java)
            R.id.btnWeather -> Intent(this, WeatherActivity::class.java)
            R.id.btnContact -> Intent(this, ContactActivity::class.java)
            R.id.btnBgThread -> Intent(this, BackgroundThreadActivity::class.java)
            R.id.btnContactTab -> Intent(this, ContactTabActivity::class.java)
            R.id.btnMyContact -> Intent(this, MyContactActivity::class.java)
            R.id.btnMyUser -> Intent(this, MyUserActivity::class.java)
            R.id.btnProfileLogin -> Intent(this, MyProfileLoginActivity::class.java)
            R.id.btnToProfile -> Intent(this, MyProfileActivity::class.java)

            else -> Intent(this, OperationActivity::class.java)
        }

        startActivity(i)
    }
}
