package id.co.iconpln.controlflowapp

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import id.co.iconpln.controlflowapp.model.Person
import kotlinx.android.synthetic.main.activity_intent.*

class IntentActivity : AppCompatActivity(), View.OnClickListener {

    private val REQUEST_CODE = 110

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intent)

        setOnClickButton()
    }

    private fun setOnClickButton() {
        btn_move_activity.setOnClickListener(this)
        btn_activity_data.setOnClickListener(this)
        btn_activity_bundle.setOnClickListener(this)
        btn_activity_object.setOnClickListener(this)
        btn_intent_result.setOnClickListener(this)
        btn_implicit.setOnClickListener(this)
        btn_open_web.setOnClickListener(this)
        btn_send_sms.setOnClickListener(this)
        btn_show_map.setOnClickListener(this)
        btn_share_text.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btn_move_activity -> {
                val intent = Intent(this, StyleActivity::class.java)
                startActivity(intent)
            }
            R.id.btn_activity_data -> {
                val intentWithData = Intent(this, IntentWithData::class.java)
                intentWithData.putExtra(IntentWithData.EXTRA_NAME, "Rachmat")
                intentWithData.putExtra(IntentWithData.EXTRA_AGE, 24)
                startActivity(intentWithData)
            }
            R.id.btn_activity_bundle -> {
                val intentWithBundle = Intent(this, IntentWithBundle::class.java)
                val bundle = Bundle()
                bundle.putString(IntentWithBundle.EXTRA_NAME, "Rachmat")
                bundle.putInt(IntentWithBundle.EXTRA_AGE, 24)
                intentWithBundle.putExtras(bundle)
                startActivity(intentWithBundle)
            }
            R.id.btn_activity_object -> {
                val person = Person("Rachmat", 24, "robertto.rachmat@gmail.com", "Jogja")
                val intentWithObject = Intent(this, IntentWithObject::class.java)

                intentWithObject.putExtra(IntentWithObject.PERSON, person)
                startActivity(intentWithObject)

            }
            R.id.btn_implicit -> {
                val phone = "123145245345"
                val dialPhoneIntent = Intent(Intent.ACTION_DIAL, Uri.parse("tel: $phone"))

                startActivity(dialPhoneIntent)
            }
            R.id.btn_open_web -> {
                val webPage = Uri.parse("https://www.binar.co.id")
                val intentWeb = Intent(Intent.ACTION_VIEW, webPage)

                if (intentWeb.resolveActivity(packageManager) != null)
                    startActivity(intentWeb)
            }
            R.id.btn_send_sms -> {
                val phoneNumber = "081121214343"
                val sendSms = Uri.parse("smsto: $phoneNumber")
                val message = "This message is set by intent"

                val intentSms = Intent(Intent.ACTION_SENDTO, sendSms)
                intentSms.putExtra("sms_body", message)
                if (intentSms.resolveActivity(packageManager) != null)
                    startActivity(intentSms)
            }
            R.id.btn_show_map -> {
                val lat = "47.6"
                val long = "-122.3"
                val showMap = Uri.parse("geo: $lat, $long")
                val intentMap = Intent(Intent.ACTION_VIEW, showMap)

                if (intentMap.resolveActivity(packageManager) != null)
                    startActivity(intentMap)
            }
            R.id.btn_share_text -> {
                val sharedText = "This text are shared by intent"
                val shareIntent = Intent(Intent.ACTION_SEND)

                shareIntent.putExtra(Intent.EXTRA_TEXT, sharedText)
                shareIntent.type = "text/plain"

                val intentShare = Intent.createChooser(shareIntent, "Share with")

                if (intentShare.resolveActivity(packageManager) != null)
                    startActivity(intentShare)

            }
            else -> {
                val intent = Intent(this, IntentWithResult::class.java)

                startActivityForResult(intent, REQUEST_CODE)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_CODE) {
            if (resultCode == IntentWithResult.RESULT_CODE) {
                val selectedValue = data?.getIntExtra(IntentWithResult.EXTRA_VALUE, 0)
                tvIntentResult.text = "Hasilnya ${selectedValue.toString()}"
            }
        }
    }
}
