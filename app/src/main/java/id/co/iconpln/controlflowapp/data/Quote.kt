package id.co.iconpln.controlflowapp.data

class Quote(private val quoteText: String, private val author: String) {
    override fun toString(): String {
        return "$quoteText - $author"
    }
}