package id.co.iconpln.controlflowapp.myProfileRegister

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import id.co.iconpln.controlflowapp.model.myProfile.ProfileRegisterResponse
import id.co.iconpln.controlflowapp.model.myProfile.RegisterResponse
import id.co.iconpln.controlflowapp.model.myProfile.RegisterUser
import id.co.iconpln.controlflowapp.network.MyProfileNetworkRepository

class ProfileRegisterViewModel : ViewModel() {

    fun doRegister(registerUser: RegisterUser): MutableLiveData<ProfileRegisterResponse<RegisterResponse>> {
        return MyProfileNetworkRepository().register(registerUser)
    }
}