package id.co.iconpln.controlflowapp.myContact

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.model.myContact.ContactResponse
import kotlinx.android.synthetic.main.item_list_contact.view.*

class MyContactAdapter : RecyclerView.Adapter<MyContactAdapter.MyContactViewHolder>() {

    private val myContactData = ArrayList<ContactResponse>()

    inner class MyContactViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: ContactResponse) {
            itemView.tvContactName.text = item.name
            itemView.tvContactEmail.text = item.email
            itemView.tvContactMobile.text = item.phone.mobile
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyContactViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_list_contact, parent, false)

        return MyContactViewHolder(view)
    }

    override fun getItemCount(): Int {
        return myContactData.size
    }

    override fun onBindViewHolder(holder: MyContactViewHolder, position: Int) {
        holder.bind(myContactData[position])
    }

    fun setData(item: ArrayList<ContactResponse>) {
        myContactData.clear()
        myContactData.addAll(item)
        notifyDataSetChanged()
    }
}