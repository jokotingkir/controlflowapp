package id.co.iconpln.controlflowapp.model.myProfile

data class RegisterUser(
    val email: String,
    val password: String,
    val name: String,
    val phone: String
)