package id.co.iconpln.controlflowapp.model.myProfile

data class ProfileRegisterResponse<T>(
    val data: RegisterResponse,
    val messages: List<String>,
    val status: Int,
    val success: Boolean
)