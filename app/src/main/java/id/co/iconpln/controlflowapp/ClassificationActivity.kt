package id.co.iconpln.controlflowapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_classification.*

class ClassificationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_classification)

        val n = et_nilai_classification.text

        btn_show_classification.setOnClickListener {
            if (n.isNotEmpty()) {
                doClassification(n.toString().toInt())
            } else {
                showToast("Nilai tidak boleh kosong")
            }

        }
    }

    private fun doClassification(nilai: Int) {
        if (nilai > 1000) {
            showToast("Nilai tidak boleh lebih dari 1000")
        } else {
            result_text_classification.text = when (nilai) {
                in 81..100 -> "Lulus Banget"
                in 71..80 -> "Lulus Aja"
                in 0..70 -> "Tidak Lulus"
                else -> "Nilai Error"
            }
        }
    }

    private fun showToast(msg: String) {
        Toast.makeText(
            this,
            msg,
            Toast.LENGTH_LONG
        ).show()
    }
}
