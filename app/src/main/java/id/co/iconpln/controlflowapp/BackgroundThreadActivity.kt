package id.co.iconpln.controlflowapp

import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.view.View
import kotlinx.android.synthetic.main.activity_background_thread.*
import kotlinx.coroutines.*
import java.lang.Runnable
import java.lang.ref.WeakReference
import java.net.URL

class BackgroundThreadActivity : AppCompatActivity(), View.OnClickListener,
    ContactAsyncTaskCallBack {

    override fun onPreExecute() {
        pbThreadAsyncProgress.visibility = View.VISIBLE
        tvThreadAsyncResult.visibility = View.GONE
    }

    override fun onProgressUpdate(vararg values: Int?) {

    }

    override fun onPostExecute(result: String?) {
        pbThreadAsyncProgress.visibility = View.GONE
        tvThreadAsyncResult.visibility = View.VISIBLE
        tvThreadAsyncResult.text = result
        tvThreadAsyncResult.layoutParams.height = 250
    }

    class FetchContactAsyncTask(listener: ContactAsyncTaskCallBack) :
        AsyncTask<URL, Int, String>() {
        //using WeakReference to avoid Memory Leak in AsyncTask
        private val contactListener: WeakReference<ContactAsyncTaskCallBack> =
            WeakReference(listener)

        override fun doInBackground(vararg params: URL): String {
            return params[0].readText()
        }

        override fun onProgressUpdate(vararg values: Int?) {
            super.onProgressUpdate(*values)

            val myListener = contactListener.get()
            myListener?.onProgressUpdate(*values)
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)

            val myListener = contactListener.get()
            myListener?.onPostExecute(result)
        }

        override fun onPreExecute() {
            super.onPreExecute()

            val myListener = contactListener.get()
            myListener?.onPreExecute()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_background_thread)

        setOnClickButton()
    }

    private fun setOnClickButton() {
        btnThreadWorker.setOnClickListener(this)
        btnThreadHandler.setOnClickListener(this)
        btnThreadAsyncTask.setOnClickListener(this)
        btnThreadCoroutine.setOnClickListener(this)
        btnThreadCoroutineAsync.setOnClickListener(this)
    }

    private val handler = Handler { message ->
        tvThreadHandlerResult.text = message.obj as String
        tvThreadHandlerResult.layoutParams.height = 250
        true
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnThreadWorker -> {
                /*
                Don't Call Network in Main Thread
                val contactResultText = URL("https://api.androidhive.info/contacts/").readText()
                tvThreadWorkerResult.text = contactResultText
                 */

                Thread(Runnable {
                    val contactResultText = URL("https://api.androidhive.info/contacts/").readText()
                    tvThreadWorkerResult.post {
                        tvThreadWorkerResult.text = contactResultText
                        tvThreadWorkerResult.layoutParams.height = 250
                    }
                }).start()
            }

            R.id.btnThreadHandler -> {
                Thread(Runnable {
                    val contactResultText = URL("https://api.androidhive.info/contacts/").readText()
                    val msg = Message.obtain()
                    msg.obj = contactResultText
                    msg.target = handler
                    msg.sendToTarget()
                }).start()
            }

            R.id.btnThreadAsyncTask -> {
                val contactResultText = URL("https://api.androidhive.info/contacts/")
                FetchContactAsyncTask(this).execute(contactResultText)
            }

            R.id.btnThreadCoroutine -> {
                runBlocking {
                    launch {
                        delay(1000)
                        tvThreadCoroutineResult.text = "Coroutine!"
                    }
                }
            }

            R.id.btnThreadCoroutineAsync -> {
                runBlocking {
                    //                    val numberAsync = async { getNumber() }
//                    val result = numberAsync.await()
//                    tvThreadCoroutineAsync.text = result.toString()

                    val contactAsync = async { getContact() }
                    val result = contactAsync.await()
                    tvThreadCoroutineAsync.text = result.toString()
                }
            }
        }
    }

    private suspend fun getContact(): String {
        return withContext(Dispatchers.IO) {
            URL("https://api.androidhive.info/contacts/").readText()
        }
    }

    private suspend fun getNumber(): Int {
        delay(1000)
        return 3 * 2
    }
}

interface ContactAsyncTaskCallBack {
    fun onPreExecute()
    fun onProgressUpdate(vararg values: Int?)
    fun onPostExecute(result: String?)
}
