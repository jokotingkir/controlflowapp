package id.co.iconpln.controlflowapp.model.myProfile

data class ProfileLoginBaseResponse<T>(
    var data: ProfileLoginResponse<Any?>?,
    var messages: List<String>,
    var status: Int,
    var success: Boolean
)