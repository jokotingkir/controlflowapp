package id.co.iconpln.controlflowapp.myUser

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.database.FavoriteUser
import id.co.iconpln.controlflowapp.model.myUser.UserDataResponse
import kotlinx.android.synthetic.main.item_list_user.view.*

class MyUserFavoriteAdapter : RecyclerView.Adapter<MyUserFavoriteAdapter.MyUserViewHolder>() {

    private var myUserData = emptyList<FavoriteUser>()
    private lateinit var onItemClickCallback: OnItemClickCallback

    inner class MyUserViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: FavoriteUser) {
            itemView.tvUserName.text = item.userName
            itemView.tvUserAddress.text = item.userAddress
            itemView.tvUserMobile.text = item.userPhone
        }
    }

    fun setOnItemClickCallback(onItemClickCallback: OnItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback
    }

    interface OnItemClickCallback {
        fun onItemClick(user: FavoriteUser)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyUserViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_list_user, parent, false)

        return MyUserViewHolder(view)
    }

    override fun getItemCount(): Int {
        return myUserData.size
    }

    override fun onBindViewHolder(holder: MyUserViewHolder, position: Int) {
        holder.bind(myUserData[position])
        holder.itemView.setOnClickListener { onItemClickCallback.onItemClick(myUserData[holder.adapterPosition]) }
    }

    fun setData(items: List<FavoriteUser>) {
        val listFavUser = ArrayList<FavoriteUser>()

        for (i in 0 until items.size) {
            listFavUser.add(items[i])
        }

        myUserData = listFavUser
        notifyDataSetChanged()
    }
}