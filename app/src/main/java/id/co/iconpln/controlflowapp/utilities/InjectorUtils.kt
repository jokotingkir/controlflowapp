package id.co.iconpln.controlflowapp.utilities

import id.co.iconpln.controlflowapp.data.FakeDatabase
import id.co.iconpln.controlflowapp.data.QuoteRepository
import id.co.iconpln.controlflowapp.ui.quotes.QuotesViewModelFactory

object InjectorUtils {

    fun provideQuotesViewModelFactory(): QuotesViewModelFactory {
        val quoteRepository = QuoteRepository.getInstance(FakeDatabase.getInstance().quoteDao)
        return QuotesViewModelFactory(quoteRepository)
    }
}