package id.co.iconpln.controlflowapp.myUserForm

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.database.FavoriteUser
import id.co.iconpln.controlflowapp.database.FavoriteViewModel
import id.co.iconpln.controlflowapp.model.myUser.UserDataResponse
import kotlinx.android.synthetic.main.activity_my_user_form.*

class MyUserFormActivity : AppCompatActivity(), View.OnClickListener {

    companion object {
        const val EXTRA_USER = "extra_user"
        const val EXTRA_USER_EDIT = "extra_user_edit"
        const val EXTRA_USER_ID = "extra_user_id"
    }

    private lateinit var user: UserDataResponse
    private lateinit var myUserFormVm: MyUserFormVM
    private lateinit var favoriteViewModel: FavoriteViewModel

    private var isEditUser = false
    private var userId: Int? = null

    private var menu: Menu? = null
    private var isFavorite = false

    private var favoriteUserId: Long? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_user_form)

        initViewModel()
        setOnClickListener()
        getIntentExtra()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_favorite, menu)
        this.menu = menu
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        setFavorite(item.itemId)
        return super.onOptionsItemSelected(item)
    }

    private fun setFavorite(itemId: Int) {
        if (itemId == R.id.itemUnfavorite) {
            if (llMyUserFormContent.visibility == View.GONE) {
                Toast.makeText(this, "Can't add to Favorite", Toast.LENGTH_SHORT).show()
            } else {
                if (!isFavorite) {
                    this.menu?.getItem(0)?.icon =
                        ContextCompat.getDrawable(this, R.drawable.ic_favorite)
                    Toast.makeText(this, "Added to Favorite", Toast.LENGTH_SHORT).show()
                    isFavorite = true
                    addOrRemoveFavorite()
                } else {
                    this.menu?.getItem(0)?.icon =
                        ContextCompat.getDrawable(this, R.drawable.ic_unfavorite)
                    Toast.makeText(this, "Remove from Favorite", Toast.LENGTH_SHORT).show()
                    isFavorite = false
                    removeFromFavorite()
                }
            }
        }
    }

    private fun addOrRemoveFavorite() {
        if (isFavorite) {
            addToFavorite()
        } else {
            removeFromFavorite()
        }

        favoriteViewModel.getAllFavoriteUsers().observe(this, Observer { listFavUser ->
            if (listFavUser.isNotEmpty()) {
                for (i in 0 until listFavUser.size) {
                    Log.d("Rachmat", "${listFavUser[i].userId} - ${listFavUser[i].userName}")
                }
            }
        })
    }

    private fun addToFavorite() {
        favoriteViewModel.insertUser(
            FavoriteUser(
                0,
                etUserFormAddress.text.toString(),
                userId.toString(),
                etUserFormName.text.toString(),
                etUserFormHp.text.toString()
            )
        )
    }

    private fun removeFromFavorite() {
        if (userId != null) {
            favoriteViewModel.deleteUser(userId as Int)
        }
        favoriteUserId = null
    }

    private fun initViewModel() {
        myUserFormVm = ViewModelProvider(
            this, ViewModelProvider.NewInstanceFactory()
        ).get(MyUserFormVM::class.java)

        favoriteViewModel =
            ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory(application))
                .get(FavoriteViewModel::class.java)
    }

    private fun getIntentExtra() {
        /*
        user = if (intent.hasExtra(EXTRA_USER)) {
            intent.getParcelableExtra(EXTRA_USER) as UserDataResponse
        } else {
            UserDataResponse("", 0, "", "")
        }
         */

        userId = intent.getIntExtra(EXTRA_USER_ID, 0)
        isEditUser = intent.getBooleanExtra(EXTRA_USER_EDIT, false)

        if (!isEditUser) {
            btnUserFormSave.visibility = View.GONE
            btnUserFormDelete.visibility = View.GONE
            menu?.findItem(0)?.isVisible = false
            populateFormData(UserDataResponse("", 0, "", ""))
        } else {
            btnUserFormAdd.visibility = View.GONE
            getUser(userId as Int)
        }
    }

    private fun setIsFavoriteUser(id: Int) {
        favoriteViewModel.getFavoriteUser(id).observe(this, Observer { user ->
            isFavorite = user != null
            if (isFavorite) {
                this.menu?.getItem(0)?.icon =
                    ContextCompat.getDrawable(this, R.drawable.ic_favorite)

                favoriteUserId = user.favUserId
            } else {
                this.menu?.getItem(0)?.icon =
                    ContextCompat.getDrawable(this, R.drawable.ic_unfavorite)
            }
        })
    }

    private fun setOnClickListener() {
        btnUserFormSave.setOnClickListener(this)
        btnUserFormDelete.setOnClickListener(this)
        btnUserFormAdd.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnUserFormSave -> {
                if (userId != null) {
                    val updateUserData = UserDataResponse(
                        etUserFormAddress.text.toString(),
                        userId ?: 0,
                        etUserFormName.text.toString(),
                        etUserFormHp.text.toString()
                    )
                    updateUser(userId as Int, updateUserData)
                }
            }
            R.id.btnUserFormDelete -> {
                if (userId != null) {
                    deleteUser(userId as Int)
                }
            }
            else -> {
                userId = 0
                val newUser = UserDataResponse(
                    etUserFormAddress.text.toString(),
                    userId ?: 0,
                    etUserFormName.text.toString(),
                    etUserFormHp.text.toString()
                )
                showLoading(true)
                addUser(newUser)
                showLoading(false)
            }
        }
    }

    private fun updateUser(id: Int, userData: UserDataResponse) {
        myUserFormVm.updateUser(id, userData).observe(this, Observer { response ->
            if (response != null) {
                Toast.makeText(this, "User updated successfully", Toast.LENGTH_SHORT).show()
                updateFavoriteUser(userData)
            } else {
                Toast.makeText(this, "Failed to update user", Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun updateFavoriteUser(userData: UserDataResponse) {
        if (favoriteUserId != null) {
            favoriteViewModel.updateUser(
                FavoriteUser(
                    favoriteUserId as Long,
                    userData.address,
                    userData.id.toString(),
                    userData.name,
                    userData.phone
                )
            )
        }
    }

    private fun deleteUser(id: Int) {
        myUserFormVm.deleteUser(id).observe(this, Observer { response ->
            if (response != null) {
                Toast.makeText(this, "User deleted successfully", Toast.LENGTH_SHORT).show()

                if (isFavorite) removeFromFavorite()
                finish()
            } else {
                Toast.makeText(this, "Failed to delete user", Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun addUser(userData: UserDataResponse) {
        myUserFormVm.createUser(userData).observe(this, Observer { response ->
            if (response != null) {
                Toast.makeText(this, "User created successfully", Toast.LENGTH_SHORT).show()
                finish()
            } else {
                Toast.makeText(this, "Failed to create user", Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun getUser(id: Int) {
        myUserFormVm.getUser(id).observe(this, Observer { response ->
            if (response != null) {
                Toast.makeText(this, "User loaded successfully", Toast.LENGTH_SHORT).show()
                populateFormData(response)
                showLoading(false)
                setIsFavoriteUser(id)
            } else {
                Toast.makeText(this, "Failed to load user", Toast.LENGTH_SHORT).show()
                showLoading(false)
            }
        })
    }

    private fun populateFormData(user: UserDataResponse) {
        etUserFormName.setText(user.name)
        etUserFormAddress.setText(user.address)
        etUserFormHp.setText(user.phone)
    }

    private fun showLoading(show: Boolean) = if (show) {
        pbUserFormLoading.visibility = View.VISIBLE
        llMyUserFormContent.visibility = View.GONE
    } else {
        pbUserFormLoading.visibility = View.GONE
        llMyUserFormContent.visibility = View.VISIBLE
    }
}
