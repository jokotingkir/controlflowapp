package id.co.iconpln.controlflowapp.myProfileLogin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.model.myProfile.ProfileLoginResponse
import id.co.iconpln.controlflowapp.model.myProfile.ProfileLoginUser
import id.co.iconpln.controlflowapp.model.myProfile.ProfileResponse
import id.co.iconpln.controlflowapp.myProfileRegister.MyProfileRegisterActivity
import kotlinx.android.synthetic.main.activity_login.btnLogin
import kotlinx.android.synthetic.main.activity_my_profile_login.*

class MyProfileLoginActivity : AppCompatActivity(), View.OnClickListener {

    companion object {
        const val EXTRA_PROFILE_RESULT = "extra"
        const val RESULT_CODE = 201
    }

    private lateinit var profileLoginVm: MyProfileLoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_profile_login)

        setOnClickListener()
        initViewModel()
    }

    private fun initViewModel() {
        profileLoginVm = ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(MyProfileLoginViewModel::class.java)
    }

    private fun setOnClickListener() {
        btnLogin.setOnClickListener(this)
        tvProfileLoginRegister.setOnClickListener(this)
    }

    private fun doLogin(profileLoginUser: ProfileLoginUser) {
        showLoading(true)
        profileLoginVm.login(profileLoginUser).observe(this, Observer { result ->
            val errorMessage = MyProfileLoginViewModel.errorMessage
            if (result != null) {
                Toast.makeText(
                    this,
                    "${result.messages[0]} :\n ${result.data?.customer?.email}",
                    Toast.LENGTH_LONG
                ).show()

                val profile = ProfileLoginResponse<ProfileResponse?>(
                    result.data?.customer,
                    result.data!!.token
                )
                openProfilePage(profile)
            } else {
                if (errorMessage.isEmpty()) {
                    Toast.makeText(this, "Network Failure", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show()
                }
            }
            MyProfileLoginViewModel.errorMessage = ""
            showLoading(false)
        })
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnLogin -> {
                val newAuth = ProfileLoginUser(
                    etProfileLoginEmail.text.toString(),
                    etProfileLoginPassword.text.toString()
                )
                doLogin(newAuth)
            }
            R.id.tvProfileLoginRegister -> {
                val registerIntent = Intent(this, MyProfileRegisterActivity::class.java)
                startActivity(registerIntent)
            }
        }
    }

    private fun showLoading(show: Boolean) {
        if (show) {
            btnLogin.visibility = View.GONE
            pbLoadingLogin.visibility = View.VISIBLE
        } else {
            btnLogin.visibility = View.VISIBLE
            pbLoadingLogin.visibility = View.GONE
        }
    }

    private fun openProfilePage(profileLoginResponse: ProfileLoginResponse<ProfileResponse?>) {
        val resultIntent = Intent().putExtra(EXTRA_PROFILE_RESULT, profileLoginResponse)
        setResult(RESULT_CODE, resultIntent)
        finish()
    }
}
