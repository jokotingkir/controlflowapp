package id.co.iconpln.controlflowapp

import androidx.lifecycle.ViewModel

class OperationViewModel : ViewModel() {
    var operationResult: Double = 0.0
    var whichOperator: String = ""

    fun execute(y: Double, op: Operation) {
        operationResult = when (op) {
            is Operation.Add -> {
                op.value.plus(y)
            }
            is Operation.Divide -> {
                op.value.div(y)
            }
            is Operation.Multiply -> {
                op.value.times(y)
            }
            is Operation.Substract -> {
                op.value.minus(y)
            }
        }
    }
}