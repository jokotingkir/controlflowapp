package id.co.iconpln.controlflowapp.model.myProfile

import android.content.Context

internal class ProfileUserPreference(context: Context) {

    companion object {
        private const val PREFS_PROFILE_USER = "user"
        private const val TOKEN = "token"
    }

    private val preferences = context.getSharedPreferences(PREFS_PROFILE_USER, Context.MODE_PRIVATE)

    fun setProfileUser(value: ProfileUser) {
        val editor = preferences.edit()
        editor.putString(TOKEN, value.userToken)
        editor.apply()
    }

    fun getProfileUser(): ProfileUser {
        val model = ProfileUser()
        model.userToken = preferences.getString(TOKEN, "")
        return model
    }

    fun removeProfileUser(value: ProfileUser) {
        val editor = preferences.edit()
        editor.remove(TOKEN)
        editor.apply()
    }
}