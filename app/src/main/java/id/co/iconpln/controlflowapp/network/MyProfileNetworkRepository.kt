package id.co.iconpln.controlflowapp.network

import android.util.Log
import androidx.lifecycle.MutableLiveData
import id.co.iconpln.controlflowapp.model.myProfile.*
import id.co.iconpln.controlflowapp.myProfileLogin.MyProfileLoginViewModel
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MyProfileNetworkRepository {

    fun auth(profileLoginUser: ProfileLoginUser): MutableLiveData<ProfileLoginBaseResponse<ProfileLoginResponse<ProfileResponse>?>> {
        val authData =
            MutableLiveData<ProfileLoginBaseResponse<ProfileLoginResponse<ProfileResponse>?>>()


        NetworkConfig.profileApi().userAuth(profileLoginUser)
            .enqueue(object :
                Callback<ProfileLoginBaseResponse<ProfileLoginResponse<ProfileResponse>?>> {
                override fun onFailure(
                    call: Call<ProfileLoginBaseResponse<ProfileLoginResponse<ProfileResponse>?>>,
                    t: Throwable
                ) {
                    authData.postValue(null)
                }

                override fun onResponse(
                    call: Call<ProfileLoginBaseResponse<ProfileLoginResponse<ProfileResponse>?>>,
                    response: Response<ProfileLoginBaseResponse<ProfileLoginResponse<ProfileResponse>?>>
                ) {
                    if (response.isSuccessful) {
                        val result =
                            response.body() as ProfileLoginBaseResponse<ProfileLoginResponse<ProfileResponse>?>
                        authData.postValue(result)
                    } else {
                        when (response.code()) {
                            in 400..420 -> {
                                val error = JSONObject(response.errorBody()?.string() ?: "")
                                val errorMessage = error.getJSONArray("messages")[0].toString()
                                MyProfileLoginViewModel.errorMessage = errorMessage
                                Log.d(
                                    "ErrorMessage",
                                    "Response : ${response.code()} -- $errorMessage"
                                )
                            }
                            else -> {
                                MyProfileLoginViewModel.errorMessage = "Unknown Network Error"
                            }
                        }
                        authData.postValue(null)
                    }
                }

            })

        return authData
    }

    fun register(registerUser: RegisterUser): MutableLiveData<ProfileRegisterResponse<RegisterResponse>> {
        val registerData = MutableLiveData<ProfileRegisterResponse<RegisterResponse>>()

        NetworkConfig.profileApi().createUser(registerUser)
            .enqueue(object : Callback<ProfileRegisterResponse<RegisterResponse>> {
                override fun onFailure(
                    call: Call<ProfileRegisterResponse<RegisterResponse>>,
                    t: Throwable
                ) {
                    registerData.postValue(null)
                }

                override fun onResponse(
                    call: Call<ProfileRegisterResponse<RegisterResponse>>,
                    response: Response<ProfileRegisterResponse<RegisterResponse>>
                ) {
                    if (response.isSuccessful) {
                        val result = response.body() as ProfileRegisterResponse<RegisterResponse>
                        registerData.postValue(result)
                    } else {
                        Log.d("THIS", response.body().toString())
                    }
                }

            })
        return registerData
    }

    fun getUser(token: String): MutableLiveData<MyProfileBaseResponse<ProfileResponse?>> {
        val userData = MutableLiveData<MyProfileBaseResponse<ProfileResponse?>>()

        NetworkConfig.profileApi().getUser("Bearer $token")
            .enqueue(object : Callback<MyProfileBaseResponse<ProfileResponse?>> {
                override fun onFailure(
                    call: Call<MyProfileBaseResponse<ProfileResponse?>>,
                    t: Throwable
                ) {
                    userData.postValue(null)
                }

                override fun onResponse(
                    call: Call<MyProfileBaseResponse<ProfileResponse?>>,
                    response: Response<MyProfileBaseResponse<ProfileResponse?>>
                ) {
                    if (response.isSuccessful) {
                        val result = response.body() as MyProfileBaseResponse<ProfileResponse?>
                        userData.postValue(result)
                    } else {
                        val error = JSONObject(response.errorBody()!!.string())
                        val messages = error.getJSONArray("messages")
                        val arrMessage = Array<String>(messages.length()) {
                            messages.getString(it)
                        }
                        val res = MyProfileBaseResponse<ProfileResponse?>(
                            null,
                            arrMessage.asList(),
                            error.getInt("status"),
                            error.getBoolean("success")
                        )

                        userData.postValue(res)
                    }
                }

            })

        return userData
    }
}