package id.co.iconpln.controlflowapp

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    private val username = "user@mail.com"
    private val password = "password"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        btnLogin.setOnClickListener {
            validateInput()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun validateInput() {
        val isEmptyUsername = etLoginUsername.editText?.text.toString().isEmpty()
        val isEmptyPassword = etLoginPassword.text.toString().isEmpty()

        when {
            isEmptyUsername -> {
                etLoginUsername.error = "Please input username"
            }
            !etLoginUsername.editText?.text.toString().isValidEmail() -> {
                etLoginUsername.error = "Please enter valid email"
            }
            isEmptyPassword -> {
                etLoginPassword.error = "Please input password"
            }
            !etLoginPassword.text.toString().isValidPassword() -> {
                etLoginPassword.error = "Please enter valid password [min length is 7 char]"
            }
        }

        if (!isEmptyUsername && !isEmptyPassword) {
            tvLoginStatus.text =
                if (etLoginUsername.editText?.text.toString() == this.username && etLoginPassword.text.toString() == this.password) {
                    "Login Status: Success"
                } else {
                    "Login Status: Invalid username or password"
                }
        }
    }

    private fun String.isValidEmail(): Boolean = Patterns.EMAIL_ADDRESS.matcher(this).matches()

    private fun String.isValidPassword(): Boolean = this.length >= 7

}
