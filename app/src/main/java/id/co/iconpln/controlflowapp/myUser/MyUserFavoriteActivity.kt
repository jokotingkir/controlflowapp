package id.co.iconpln.controlflowapp.myUser

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.database.FavoriteUser
import id.co.iconpln.controlflowapp.database.FavoriteViewModel
import id.co.iconpln.controlflowapp.myUserForm.MyUserFormActivity
import kotlinx.android.synthetic.main.activity_my_user_favorite.*

class MyUserFavoriteActivity : AppCompatActivity() {

    private lateinit var adapter: MyUserFavoriteAdapter
    private lateinit var myUserVm: MyUserVM
    private lateinit var favoriteViewModel: FavoriteViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_user_favorite)

        initViewModel()
        showMyFavoriteList()
        fetchFavoriteData()
        addListClickListener()
    }

    private fun addListClickListener() {
        adapter.setOnItemClickCallback(object : MyUserFavoriteAdapter.OnItemClickCallback {
            override fun onItemClick(user: FavoriteUser) {
                openUserForm(user)
            }

        })
    }

    override fun onResume() {
        super.onResume()
        favoriteViewModel.getAllFavoriteUsers()
    }

    private fun openUserForm(user: FavoriteUser) {
        val userFormIntent = Intent(this@MyUserFavoriteActivity, MyUserFormActivity::class.java)

        userFormIntent.putExtra(MyUserFormActivity.EXTRA_USER_ID, user.userId.toInt())
        userFormIntent.putExtra(MyUserFormActivity.EXTRA_USER_EDIT, true)
        startActivity(userFormIntent)
    }

    private fun fetchFavoriteData() {
        favoriteViewModel.getAllFavoriteUsers().observe(this, Observer { listFavUser ->
            if (listFavUser.isNotEmpty()) {
                adapter.setData(listFavUser)
            }
        })
    }

    private fun showMyFavoriteList() {
        adapter = MyUserFavoriteAdapter()
        adapter.notifyDataSetChanged()

        rvMyFavoriteList.layoutManager = LinearLayoutManager(this)
        rvMyFavoriteList.adapter = adapter
    }

    private fun initViewModel() {
        favoriteViewModel =
            ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory(application))
                .get(FavoriteViewModel::class.java)
    }


}
