package id.co.iconpln.controlflowapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.activity_volume.*

class VolumeActivity : AppCompatActivity() {

    private val tvResult: TextView
        get() = tv_result

    private val btnCalculate: Button
        get() = btn_calculate

    private val etLength: EditText
        get() = edt_length

    private val etWidth: EditText
        get() = edt_width

    private val etHeight: EditText
        get() = edt_height

    private lateinit var volumeViewModel: VolumeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_volume)

        initViewModel()
        displayResult()
        setClickListener()
    }

    private fun initViewModel() {
        volumeViewModel = ViewModelProviders.of(this).get(VolumeViewModel::class.java)
    }

    private fun displayResult() {
        tvResult.text = volumeViewModel.volumeResult.toString()
    }

    private fun setClickListener() {
        btnCalculate.setOnClickListener {
            val length = etLength.text.toString()
            val width = etWidth.text.toString()
            val height = etHeight.text.toString()

            when {
                length.isEmpty() -> etLength.error = getString(R.string.empty_field)
                width.isEmpty() -> etWidth.error = getString(R.string.empty_field)
                height.isEmpty() -> etHeight.error = getString(R.string.empty_field)
                else -> {
                    volumeViewModel.calculate(length, width, height)
                    displayResult()
                }
            }
        }

    }
}
