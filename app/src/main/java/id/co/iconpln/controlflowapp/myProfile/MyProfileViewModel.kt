package id.co.iconpln.controlflowapp.myProfile

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import id.co.iconpln.controlflowapp.model.myProfile.*
import id.co.iconpln.controlflowapp.network.MyProfileNetworkRepository

class MyProfileViewModel : ViewModel() {
    fun getUser(token: String): MutableLiveData<MyProfileBaseResponse<ProfileResponse?>> {
        return MyProfileNetworkRepository().getUser(token)
    }
}